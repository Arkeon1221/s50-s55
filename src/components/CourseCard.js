import {useEffect, useState} from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

export default function CourseCard({course}) {

  // Destructuring the props
    const {name, description, price, _id} = course

  // Initialize a 'count' state with a value of (0)
    const [count, setCount] = useState(0)
    const [slots, setSlot] = useState(15)
    const [isOpen, setIsOpen] = useState(true)

    // function enroll(){

    //   if(slots > 0){

    //     setCount(count + 1)
    //     setSlot(slots -1)
    //   } else {

    //      alert("The enrollment slots is already full!");
    //   }

    // }

    // // Effects in React is just like side effect/effects in real life, where everytime something happens whithin the component, a function or condition runs.
    // // You may also listen or watch a specific state for changes instead of watching/ listening to the whole component

    //  useEffect(() => {
    //     if(slots === 0) {
    //       setIsOpen(false)
    //     }

    //   }, [slots])

    return (
      <Row className="mt-3 mb-3" >
      <Col xs={12} >

      <Card className="cardCourses p-3" >
        <Card.Body>
          <h2>{name}</h2>
          <Card.Title>Description</Card.Title>
          <Card.Text>
            {description}
          </Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>Php {price}</Card.Text>
          <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
        </Card.Body>
      </Card>
      </Col>
      </Row>
    );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the props and assign specific validation for each of them
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}